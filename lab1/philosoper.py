import time
import random
import multiprocessing as mp
import heapq
import itertools as it

REQ = 'request'
RSP = 'respons'
END = 'goodbye'
def philosopher(phid, pipes_dict):
    random.seed(time.time() + phid)
    pq = []
    clock = 0
    time.sleep(random.uniform(0.1, 2))
    clock += 1
    old_clock = 1
    # send requests to other processes
    message = (clock, phid, REQ)
    heapq.heappush(pq, message)
    for i, p in pipes_dict.items():
        print(f"{phid} {message}")
        p.send(message)

    # wait and evaluate other requests
    for i, p in pipes_dict.items():
        recieved = p.recv()
        pclock, pid, msg = recieved
        assert msg == REQ
        #print(f"{phid}\trecv {msg} from {pid} at {pclock} ({clock})")
        clock = max(pclock, clock) + 1
        heapq.heappush(pq, recieved)
        message = (clock, pid, RSP)
        print("{}\tsending {}".format(phid, message))
        p.send(message)

    for i, p in pipes_dict.items():
        recieved = p.recv()
        pclock, pid, msg = recieved
        assert msg == RSP
        print(f"{phid}\t{msg} from {pid} at {pclock} (clock)")

    while pq[0][1] != phid:
        print(f"{phid}\t{pq}")
        pid = pq[0][1]
        recieved = pipes_dict[pid].recv()
        plock, pid, msg = recieved
        assert msg == END
        heapq.heappop(pq)


    print(f"\t\033[91mPHILOSOPHER {phid} IS AT THE TABLE!\033[0m")
    time.sleep(3)
    heapq.heappop(pq)
    for i, p in pipes_dict.items():
        message = (old_clock, phid, END)
        p.send(message)
    print(f"Philosopher {phid} ended")
    return

if __name__ == "__main__":
    N = 5
    pipes = {i: dict() for i in range(N)}

    for i, j in it.combinations(range(N), 2):
        # pipe1, pipe2 are two ends of a bidirectional pipe
        pipe1, pipe2 = mp.Pipe()
        pipes[i].update({j: pipe1})
        pipes[j].update({i: pipe2})

    processes = [mp.Process(target=philosopher, args=(i, pipes[i])) for i in range(N)]

    for p in processes:
        p.start()
    for p in processes:
        p.join()

