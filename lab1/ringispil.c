/**
 * The carrousel starts accepting visitors, but it's only got a limited capacity.
 * A few of the first get approved to go for a ride, where they are instructed to sit.
 * A ride of a lifetime then commences, giving visitors the feeling they all came for.
 * After the ride ends, visitors are instructed to get up and make room for other visitors. 
 * (They won't give up though).
 * This whole story is then repeated while there are any interested visitors left.
 */

// bunch of includes
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

// message types for communication
long RIDE = 0x100000000;
long SIT = 0x200000000;
long GET_UP = 0x400000000;

// message buffer structure
typedef struct msgbuf {
    long type;
    char text[200];
} msgbuf;

// A form of message that's being sent
char REQUEST_RIDE[] = "Visitor %d would like to ride.";

int main(int argc, char *argv[]) {
    srand(time(0)); // set new random every time
    setbuf(stdout, NULL); // when printing, just print it directly, no buffering.
    msgbuf buffer;
    int key = getuid();
    int msqid = msgget(key, 0600 | IPC_CREAT); // create message queue. remember the id.
    if (msqid == -1) exit(1);

    // because arguments to the programs must be passed as a string only,
    // we reserve some space to convert integers to strings
    char msqid_string[5];
    char visitor_id[5];
    char seed[30];
    sprintf(msqid_string, "%d", msqid);

    char *process_count_string = getenv("SMETKO_VISITOR_COUNT");
    int process_count = process_count_string == NULL ? 8 : atoi(process_count_string);
    char *repeat_rides = getenv("SMETKO_VISITOR_REPEAT");
    repeat_rides = repeat_rides == NULL ? "3" : repeat_rides;
    for (int i = 0; i < process_count; i++) {
        // make `process_count` visitors
        switch(fork()) {
            case -1:
                printf("The process could not be started.\n");
                exit(1);
                break;
            
            case 0:
                // first convert some arguments to strings
                sprintf(visitor_id, "%d", i);
                sprintf(seed, "%d", rand());
                // then start a program
                // send them their id, the id of a queue, one message, a random seed and the number of ride repeats
                execl("./visitor", "./visitor", visitor_id, msqid_string, REQUEST_RIDE, seed, repeat_rides, NULL);

                // if the code reaches here, there ought to be some error.
                exit(1);
                break;
            
            default:
                break;
        }
    }
    // capacity of the carrousel
    char *capacity_string = getenv("SMETKO_CARROUSEL_CAPACITY");
    int capacity = capacity_string == NULL ? 4 : atoi(capacity_string); // TODO env
    struct msqid_ds stats;
    int accepted[capacity];
    do {
        printf("\nNew ride:\n\n");
        for (int i = 0; i < capacity; i++) {
            // accept `capacity` visitors
            if (msgrcv(msqid, (msgbuf *)&buffer, sizeof(buffer) - sizeof(long), RIDE, 0) == -1) {
                printf("Error while reading from queue.\n");
                exit(1);
            }
            int id_approved;
            sscanf(buffer.text, REQUEST_RIDE, &id_approved);
            printf("Vistor %d approved\n", id_approved);
            // remember each one and tell the to sit down
            accepted[i] = id_approved;
            buffer.type = (SIT | id_approved);
            sprintf(buffer.text, "Sit %d.", id_approved);
            if (msgsnd(msqid, &buffer, strlen(buffer.text), 0) == -1) exit(1);
        }

        // WEEEE!
        printf("A ride started.\n");
        usleep((3000 + 2000*rand()/RAND_MAX) * 1000);
        printf("A ride ended.\n");

        // tell the nauseus visitors to get up and make room for more #profit #capitalism
        for (int i = 0; i < capacity; i++) {
            buffer.type = GET_UP | accepted[i];
            sprintf(buffer.text, "Get up.");
            if (msgsnd(msqid, &buffer, strlen(buffer.text), 0) == -1) exit(1);
        }

        // let's see if there are any visitors wanting more
        // look at the queue and store the result in &stats
        msgctl(msqid, IPC_STAT, &stats);
    } while (stats.msg_qnum > 0);

    printf("Smetko's CARROUSEL (TM) is taking a break until someone runs ./ringispil again.\n");

    // nice. free system resources
    msgctl(msqid, IPC_RMID, NULL);
    return 0;
}
