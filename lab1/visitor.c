/**
 * Visitor requests a ride with RIDE (0x100000000) sending its ID as a message.
 * Visitor then waits for a message with type SIT (0x200000000) or'd with its ID (for example 0x200000003, if ID == 3).
 * After recieving (SIT | ID), the visitor is sitting and waiting for a ride. It waits for GET_UP (also or'd with ID).
 * Then, a visitor wants to do it all again! Until it passes out or barfs all over the carrousel owner,
 * which ever comes first.
 */

// bunch of includes
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

// message types for communication
long RIDE = 0x100000000;
long SIT = 0x200000000;
long GET_UP = 0x400000000;

// message buffer structure
typedef struct msgbuf {
    long type;
    char text[200];
} msgbuf;

int main(int argc, char *argv[]) {
    setbuf(stdout, NULL); // when printing, just print it directly, no buffering
    // cast bunch of string back to ints
    int self = atoi(argv[1]);
    int msqid = atoi(argv[2]);
    char *request_message = argv[3];
    // set random seed to something other than time(NULL)
    // this is due to bunch of processes spawning in a short time
    // therefore they are likely to end up with a similar or the same seed
    // which is bad
    srand(atoi(argv[4]));
    msgbuf buf;
    int ride_count = atoi(argv[5]);
    for (int i = 0; i < ride_count; i++ ) {
        usleep((100 + 1900*rand()/RAND_MAX) * 1000);
        // yell at carrousel owners you want to ride (aka send him a message)
        buf.type = RIDE;
        sprintf(buf.text, request_message, self);
        if (msgsnd(msqid, (msgbuf *)&buf, strlen(buf.text), 0) == -1) {
            printf("Error during sending a message.\n");
            exit(1);
        }
        // now listen to the rules and enjoy.
        if (msgrcv(msqid, &buf, sizeof(buf) - sizeof(long), (SIT | self), 0)) {
             printf("\tVisitor %d sat down.\n", self);
        }

        // you've enjoyed enough. Now stagger out of the carrousel.
        if (msgrcv(msqid, &buf, sizeof(buf) - sizeof(long), GET_UP | self, 0)) {
            printf("\tVisitor %d got up.\n", self);
        }
        // "So help me God, I will do it again, for I am adrenaline junkie"
    }
    // "No more please, I've had enough"
    printf("\tVisitor %d has ended.\n", self);
    return 0;
}

