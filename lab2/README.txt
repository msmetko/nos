Druga laboratorijska vjezba is Naprednih Operacijskih Sustava 2019/2020

0. Da bi se prilozene bash skripte koristile, potrebno je stvoriti neke
identitete. Oni se stvaraju naredbom create identities:

$ ./create_identities.sh alice
$ ./create_identities.sh bob

Moze i vise, ali dva su sasvim dovoljna.

Skripta ne prima nista na ulaz, ali prima tocno jedan argument, ime identiteta.
Kao izlaz stvara folder s datim imenom i unutra dvije datoteke: public.pem i
private.pem, koji su javni i privatni kljuc, tim redom.

###
UPUTE
###
- Za stvaranje digitalne omotnice stringa ili datoteke, potrebno je pozvati
skriptu ./digital_envelope.sh koja na ulaz prima string / sadrzaj datoteke.

- Za stvaranje digitalnog potpisa stringa ili datoteke, potrebno je pozvati
skriptu ./digital_signature.sh koja na ulaz prima string / sadrzaj datoteke.

- Za stvaranje digitalnog pecata stringa ili datoteke, potrebno je pozvati
skriptu ./digital_seal.sh koja na ulaz prima string / sadrzaj datoteke.

- Za verifikaciju digitalnog potpisa, potrebno je pozvati skriptu
verify_signature.sh koja na ulaz prima poruku s digitalnim potpisom

- Za dekripciju poruke u digitalnoj omotnici, potrebno je pozvati skriptu
./message_decypher.sh, koja na ulaz prima poruku sa sjednickim kljucem

- Za istovremenu verifikaciju i dekripciju, potrebno je pozvati skriptu
./decrypt.sh, koja na ulaz prima enkriptiranu poruku, enkriptirani skriveni
kljuc sjednice simetricnog algoritma te digitalni potpis.


- Za demonstraciju komunikacije izmedu dva identiteta, moguce je koristiti
skriptu communicate.sh, koja poziva skripte digitalnog pecata, a zatim i
dekripcije.

PRIMJERI:
(izlazi su odredeni stanjem generatora pseudoslucajnog generatora slucajnih
brojeva te je vjerojatnost da izlazi budu isti astronomski malene)
> defaultne vrijednosti:
> velicina kljuca 32
> hash md5
> algoritam enkripcije AES
>     - broj bitova 256
>     - nacin kriptiranja cbc
> Defaultne vrijednosti mogu se promjeniti odgovarajucim argumentima
> za detalje, skocite dolje u sekciju DOKUMENTACIJA

$ echo "jasni tekst" | ./digital_envelope.sh --reciever bob
/vXQjOdHO4DlrDpyxd/XtKFwCSXxx5RCtEZbWNb4dzM=;Htzp+C6550lQkqnhSRfcaS7rgfQWcfDNN4OpBfJYD5JnIRl36staG1e6Y7Hs1242LKk0szHXn6Kc0Nh2L2M8clpH8TdV8cmn48l7FGz9Lsoq27MDN1HzX59ELhM+/g5N2E4TP1zCxUIrwyfX0o92QU+pybngnBxrCyhBHF5RzN4=
$ echo "jasni tekst" | ./digital_envelope.sh -r bob | ./digital_signature --sender alice
EruqD78+xKsDtjpk05V8mcReOi9k3EfpH316X25LW/g=;Cuk0EIB/ijdh+GZtrVIZzlr7EETPrM2dxPhYE6RVomTHGoaHyWvblt/z4QUOSWnSxhuyLUWpqX374RxLz8z4S7EImENFsSUc8rN59cO8i7L7/3/O4PKy2hYWRs2IhUDpR7t5miWsZ+9DDAuYbcUwFQCtXSWrZehJzLdCF2WyH9A=;Ny5QFYTnKv8O1hgxVnAq2M75rs+5tUFuJTrEi6eEmfPEbNRi9zdiZmbmoU0/r5uDud8O4MNZnkjDlVo+wb144o6lm69rQOW/BKIQP5ZPzUJKhXUzKyjFsEk7q5uScCCnZCrjR6BA+6c9eCFCBxPLnaw/HiVh966zlzARr8+V5io=
$ echo "jasni tekst" | ./digital_seal --sender alice --reciever bob
KI+M9TZZ+uqSHJ5puXBKzYUBs12b97bVLdozxOwBOAU=;Y8CCYksTAfe3mB5xFyKXln5OnGO53huKZuPSmigQDFgDPdaQ4IB1RfxZ+Y0Z/ohg6ExLEnE5107GJlPtVr5GlImi1F6iQgbfjoZEzJtu+bTje3HwRydHE9TyPUZTmET2qKvZdLpKYnWz6lucmVadPqay/RZpI3Jeqkit/gNt2LY=;Do4AWVWVwkuFGU0FC4+Onmyw2QVNGLuTcMrWvRT1xJ5w/s85MiqaHEpJGS6Xa1z0/z3/Mb+q+/Km4nAS9xqy+up4YJKjKHsm5YCTCFbYXXrwpkzDqrbw3CF5RsROKHv58nCocK8gcuyT8DMQmEm6o0dNZrfbma4+P5AJSF9Sb30=
$ echo "jasni tekst" | ./digital_seal --sender alice --reciever bob | ./decrypt.sh --sender alice --reciever bob
jasni tekst
$ ./communicate --from bob --to alice --message "Hejjjj :))"
Digital seal: EHap88OFsPj9yG09fXSkuNmovXxSnknsFa+73EYHjnI=;BSpY0N0Wk9Gxf3SwFEMJFjrTF05kLAaTgpYqoPuppGeB0RakalUf/vhdYv3Amg6xD8Jt0CLZPqa3e3mvAui79FBdoGjjdBDBr44GCvgwG3NSFq2Iir0irS1I1Q10D6rvhmgfSDyjlfSLU/0rdQwrDhwh/FWkIs9CNYhPV45xkqk=;X51mP2AEUlMLp0XRa3Q+qEa0XzWsPcROHsYGvSNpCuphMXydyY2SnMjzUzAHxelNh87hROeWsMyJ6LDPjXBqOTDTuN6xZDeNFNnviKmC7am4UE6CQU73hvB9S0vaESpqUn5/bSUITUkMCpMg4eV+oDO5eurseuekFe8EJwEXFLo=

alice recieved: Hejjjj :))
$ ./communicate --from alice --to bob --message "Imam decka"
Digital seal: 9e3gQRTiHVuSB0gAovLGIqRkQhE397cCp6vyD1zrKgo=;lmy2MVZPsTwpqLmoa+TtMW5ap+AnaZRgaz/64K+mzQYDvZtmBSgDSGi/v3z0kFM4A3ll3njNLSQWvF0GMFh500tXQsnJD4fhc7NbzZa0UofWIGJNAjxIi4Mfp2aJMinjxDX1hPtouBDR7Oa0ga67lM6IWCDODrC/RHrd7H/kn14=;q65Gw80DP2/BT9iook8bSsC1hhv3uVm/Yt3bA5WMnfEm3vha0hGq+lmEwDUo0O/bJidLOibyswuwcHvuBnTefR4W7Hkkqx8Z/UslgcaDFJwsYQHzVX4DsuMgckjozwvOtGhlJn5ZxBkzPBmmoCHwbip3ppYxELjvVf3XI1xEOeQ=

bob recieved: Imam decka
$

Osobno najdrazi primjer, sa dva terminala (bash1 i bash2) koja su smjestena u
istom direktoriju.

U terminalu2 pokrenuti:
bash2$ nc -N -l localhost 1204 | ./decrypt --sender alice --reciever bob > file2.txt

U terminalu1 pokrenuti:
bash1$ cat /dev/urandom | head -n 64 | base64 > file1.txt
bash1$ cat file1.txt | ./digital_seal --sender alice --reciever bob | nc -N localhost 1204

Zatim se u terminalu2 moze pokrenuti:
bash2$ diff -q file1.txt file2.txt && echo "Datoteke su iste" || echo "Datoteke su razlicite"
Datoteke su iste

Iznenadujuce, isti rezultat dobije se pokretanjem iste naredbe i u terminalu1.
Ovime je pokazano da se skripte mogu koristiti za komunikaciju i preko interneta
a uz male izmjene (koje se ticu djeljenja informacija o javnim kljucevima) bi se
mogle koristiti i u stvarnosti
###
DOKUMENTACIJA
###
1. digital_envelope.sh:
stvara digitalnu omotnicu poruke. Generira kljuc te base64 enkodiranu poruku
kriptira algoritmom sa istim kljucem.
Ulaz: stdin
argumenti:
    -r|--reciever
        ime primatelja. Naziv foldera u kojem su spremljeni kljucevi
    -a|--algorithm
        naziv algoritma. Dopustene vrijednosti: aes, des, aria, camellia
        Pretpostavljena vrijednost je aes
    -b|--bits
        broj bitova kljuca. Dopustene vrijednosti:
            - za aes, aria, camellia: 128, 192, 256
            - za des: ede, ede3
            Pretpostavljena vrijednost je 256
    -m|--mode
        nacin kriptiranja vise od jednog bloka. Dopustene vrijednosti:
            - za des: cbc, cfb, ofb
            - za ostale: {iste vrijednosti kao za des} + ctr, ecb
            Pretpostavljena vrijednost je cbc
    -v|--verbose
        Verbozan ispis, usmjeren na stderr
Izlaz: standardni izlaz. poruka je sastavljena od 2 stringa odvojena
tocka zarezom (;) - prvi dio je base64 enkodirana kriptirana poruka. Drugi dio
je base64 enkodiran simetricni kljuc sjednice, kriptiran RSA algoritmom pomocu
javnog kljuca primatelja.

2. digital_signature.sh
Digitalno potpisuje svoj ulaz pomocu RSA algoritma.
Ulaz: stdin
argumenti:
    -s|--sender
        ime posaljitelja. Naziv foldera u kojem su spremljeni kljucevi.
    -h|--hash
        Naziv hash algoritma
        Dopustene vrijednosti: md4, md5, sha1, sha224, sha256, sha3-224,
                               sha3-256, sha3-384, sha3-512, sha384, sha512
                               sha512-224, sha512-256, shake128, shake256, sm3
        Pretpostavljena vrijednost je md5.
    -v|--verbose
        Verbozan ispis usmjeren na stderr
Izlaz: dva stringa odvojena tocka-zarezom (';'). Prvi dio je ulaz bez ikakve
promjene. Drugi dio je base64 enkodiran sazetak koji je prije enkodiranja
kriptiran RSA algoritmom pomocu privatnog kljuca posaljitelja.

3. digital_seal.sh
Skripta koja digitalno potpisuje digitalnu omotnicu. Radi kompoziciju
digital_envelope.sh i digital_signature.sh.
Ulaz: stdin
argumenti: unija argumenata za ./digital_envelope.sh i ./digital_signature.sh
Izlaz: stdout

4. verify_signature.sh
Verificira digitalni potpis. Ako potpis nije uspjesan, izlazi na silu sa kodom
1. Ako potpis je uspjesan, na stdout ispisuje poruku.
Potpis se verificira tako da se RSA algoritmom dekodira potpis javnim kljucem
posaljitelja, cime se dobija hash poruke. Zatim se poruka hashira na strani
verifikatora te se usporeduju dani hashevi.

Ulaz: stdin
Argumenti: isti kao i za digital_signature.sh (s istim defaultsima)
Izlaz: ako je verifikacija prosla uspjesno, vraca poruku (sve sto je ispred
zadnje tocke-zareza (';')).

5. message_decypher.sh
Dekriptira poruku sastavljenu od dva dijela, odvojena tocka-zarezom (';'). Prvi
dio je base64 enkodirana poruka enkriptirana simetricnim algoritmom, a drugi
dio je base64 enkodirani kljuc simetricnog algoritma, enkriptiran RSA
algoritmom pomocu javnog kljuca primatelja. Prvo se kljuc sjednice dekriptira
privatnim kljucem primatelja, zatim se dobiveni kljuc sjednice iskoristi za
dekripciju prvog dijela. Rezultat (sad enkodiran u utf8, a ne vise base64)
ispisuje se direktno na stdout

Ulaz: stdin
Argumenti: isti kao i u digital_envelope.sh
Izlaz: stdout, plain text poslane poruke.

6. decrypt.sh
Radi kompoziciju verifikacije potpisa i dekriptiranja poruke. Kao ulaz prima
poruku od tri dijela: base64 enkodirani skriveni tekst kriptiran simetricnim
algoritmom, base64 enkodirani skriveni kljuc sjednice kriptiran javnim kljucem
primatelja, te base64 enkodiran hash potpisan privatnim kljucem posaljitelja.
Prvo verificira potpis, te ako je potpis uspjesno verificiran, dekriptira
poruku i prikazuje jasni tekst.

Ulaz: stdin, tekst sa tri dijela odvojena tocka-zarezom (';')
Argumenti: unija argumenata verify_signature.sh message_decypher.sh.
Izlaz: stdout, jasni tekst poslane poruke

###
KNOWN BUGS

###
- Poslani podaci idealno su obicni stringovi li tekstualne datoteke. Bilo kakvi
oblici binarnih podataka poput .pdf datoteka ili slika ne mogu se slati ovim
skriptama jer cesto sadrzavaju nul bajtove koje Bash proguta. Iako sve prode u
redu sa slanjem i dekodiranjem, u trenutku dekripcije (kljuca ili poruke)
dobiveni niz bajtova nece imati nul bajtove.
    - Ovo implicira da kad bi postojao softver koji bi znao dekriptirati prvi
    dio stringa sa sjednickim kljucem skrivenim u drugom dijelu, ali ostaviti
    nul bajtove, stvar bi funkcionirala. Autor ovog koda ovo samo sluti te nije
    isprobao.
