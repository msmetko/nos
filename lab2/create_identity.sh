#!/usr/bin/bash
if [[ $# != 1 ]]; then
    echo "Only one argument allowed: name" && exit 1;
fi
mkdir "$1"
openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:1024 -out "$1/private.pem"
openssl pkey -in "$1/private.pem" -out "$1/public.pem" -pubout

