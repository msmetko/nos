#!/usr/bin/env bash
abort() {
    echo "$1" && exit 1;
}

# DEFAULTS
algorithm='aes';
bits='256';
mode='cbc';
key_size='32';
verbose='';

# ARGUMENT PARSING
while [[ $# -gt 0 ]]; do
    key="$1";
    case $key in
        --help)
            echo 
            ;;
        -r|--reciever)
            reciever="$2";
            shift; shift;
            ;;
        -a|--algorithm)
            algorithm="$2";
            shift; shift;
            ;;
        -b|--bits)
            bits="$2";
            shift; shift;
            ;;
        -m|--mode)
            mode="$2";
            shift; shift;
            ;;
        -k|--key-size)
            key_size="$2";
            shift; shift;
            ;;
        -v|--verbose)
            verbose='1';
            shift;
            ;;
        *)
            abort "Unsupported argument: $key";
            ;;
    esac
done
if [[ $reciever == '' ]]; then
    abort "No reciever defined, aborting.";
fi
encryption_type="$algorithm-$bits-$mode";
random_key=$(openssl rand -base64 "$key_size");
if [[ $verbose ]]; then
    >&2 echo "The generated key is: $random_key";
fi
message="$(cat - | base64 -w 0)";
# encrypt message with the symmetric cypher and encode it in base64
message_encrypted=$(echo "$message" | openssl enc -e -k "$random_key" "-$encryption_type" -nosalt 2>/dev/null | base64 -w 0);

# Encrypt the symmeric key with the RSA using reciever's public key
# NOTE: even with everything else the same in every run, the variable $key_encypted will be different each time.
# That is because random padding is added each time (the default is PKCS#1)
# This cannot be turned off.
key_encrypted=$(echo "$random_key" | openssl pkeyutl -encrypt -pubin -inkey "$reciever/public.pem" | base64 -w 0);
echo "$message_encrypted;$key_encrypted";

