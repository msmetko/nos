#!/usr/bin/env bash
abort() {
    echo "$1" && exit 1;
}

# DEFAULTS
algorithm='aes';
bits='256';
mode='cbc';
hash_type='md5';
verbose='';

# ARGUMENT PARSING
while [[ $# -gt 0 ]]; do
    key="$1";
    case $key in
        --help)
            echo 
            ;;
        -h|--hash)
            hash_type="$2";
            shift; shift;
            ;;
        -r|--reciever)
            reciever="$2";
            shift; shift;
            ;;
        -s|--sender)
            sender="$2";
            shift; shift;
            ;;
        -a|--algorithm)
            algorithm="$2";
            shift; shift;
            ;;
        -b|--bits)
            bits="$2";
            shift; shift;
            ;;
        -m|--mode)
            mode="$2";
            shift; shift;
            ;;
        -v|--verbose)
            verbose='1';
            shift;
            ;;
        *)
            abort "Unsupported argument: $key";
            ;;
    esac
done
if [[ $reciever == '' ]]; then
    abort "No reciever defined, aborting.";
fi
if [[ $sender == '' ]]; then
    abort "No sender defined, aborting.";
fi

./verify_signature.sh -s $sender -h $hash_type | ./message_decypher.sh -r $reciever -a $algorithm -b $bits -m $mode
