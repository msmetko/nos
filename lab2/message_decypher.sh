#!/usr/bin/env bash
abort() {
    echo "$1" && exit 1;
}

algorithm='aes';
bits='256';
mode='cbc';
verbose='';

while [[ $# -gt 0 ]]; do
    key="$1";
    case $key in
        -r|--reciever)
            reciever="$2";
            shift; shift;
            ;;
        -a|--algorithm)
            algorithm="$2";
            shift; shift;
            ;;
        -b|--bits)
            bits="$2";
            shift; shift;
            ;;
        -m|--mode)
            mode="$2";
            shift; shift;
            ;;
        -v|--verbose)
            verbose='1';
            shift;
            ;;
        *)
            abort "Unsupported argument: $key";
            ;;
    esac
done
if [[ $reciever == '' ]]; then
    abort "No reciever defined, aborting.";
fi
encryption_type="$algorithm-$bits-$mode";
incoming="$(cat -)";
IFS=';' read -r message encypted_key <<< "$incoming"; # this sets IFS variable for this scope only

# the key is decrypted with RSA using reciever's private key
key=$(echo "$encypted_key" | base64 -d | openssl pkeyutl -decrypt -inkey "$reciever/private.pem");
# the decrypted key is then used with a symmetric algorithm to decypher the sent clear text
message=$(echo "$message" | base64 -d | openssl enc -d -k $key -$encryption_type -nosalt 2>/dev/null | base64 -d);
echo "$message";
