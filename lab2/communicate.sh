#!/usr/bin/env bash
abort() {
    echo "$1" && exit 1;
}

# DEFAULTS
mode='cbc';
algorithm='aes';
bits='256';
hash_type='sha3-512';
key_size='32';

while [[ $# -gt 0 ]]; do
    key="$1";
    case $key in
        -f|--from)
            user1="$2";
            shift; shift;
            ;;
        -t|--to)
            user2="$2";
            shift; shift;
            ;;
        -m|--message)
            message="$2";
            shift; shift;
            ;;
        --mode)
            mode="$2";
            shift; shift;
            ;;
        -a|--algorithm)
            algorithm="$2";
            shift; shift;
            ;;
        -h|--hash)
            hash_type="$2";
            shift; shift;
            ;;
        -k|--key-size)
            key_size="$2";
            shift; shift;
            ;;
        -b|--bits)
            bits="$2";
            shift; shift;
            ;;
        *)
            abort "Unsupported argument: $key";
            ;;
    esac
done

#echo "Message is: $message";
#echo "Key is: $random_key";

digital_seal=$(echo "$message" | ./digital_seal.sh -s $user1 -r $user2 -a $algorithm -b $bits -m $mode -h $hash_type -k $key_size -v);
echo "Digital seal: $digital_seal";
echo;
###
### ^ ENCRYPTER | v DECRYPTER
###

deciphered=$(echo "$digital_seal" | ./decrypt.sh -s $user1 -r $user2 -a $algorithm -b $bits -m $mode -h $hash_type -v);
echo "$user2 recieved: $deciphered";
