#!/usr/bin/env bash
abort() {
    echo "$1" && exit 1;
}

#DEFAULT
hash_type='md5';

while [[ $# -gt 0 ]]; do
    key="$1";
    case $key in
        -s|--sender)
            sender="$2";
            shift; shift;
            ;;
        -h|--hash)
            hash_type="$2";
            shift; shift;
            ;;
        *)
            abort "Unsupported argument: $key";
            ;;
    esac
done
if [[ $sender == '' ]]; then
    abort "No sender defined, aborting";
fi
recieved="$(cat -)";
signature=$(echo "$recieved" | awk -F ';' '{print $NF}');
message=$(echo "$recieved" | awk 'BEGIN{FS=";";OFS=";"}NF{NF--};1');
# hash the message on the verificator's side
message_hash=$(echo "$message" | openssl dgst -"$hash_type" -binary | base64 -w 0);
# and compare it with the decrypted key using sender's public key
signature_hash=$(echo "$signature" | base64 -d | openssl rsautl -verify -pubin -inkey "$sender/public.pem");
if [[ "$message_hash" == "$signature_hash" ]]; then
    echo "$message";
else
    abort "The signature is not valid, aborting."
fi
