#!/usr/bin/env bash
# DEFAULTS
hash_type='md5';
verbose='';

abort() {
    echo "$1" && exit 1;
}

while [[ $# -gt 0 ]]; do
    key="$1";
    case $key in
        -s|--sender)
            sender="$2";
            shift; shift;
            ;;
        -h|--hash)
            hash_type="$2";
            shift; shift;
            ;;
        -v|--verbose)
            verbose='1';
            shift;
            ;;
        *)
            abort "Unsupported argument: $key";
            ;;
    esac
done
if [[ $sender == '' ]]; then
    abort "No sender defined, aborting."
fi
message="$(cat -)";
# hash the message using some chosen algorithm
message_hash=$(echo "$message" | openssl dgst -"$hash_type" -binary | base64 -w 0);
# sign the hash with the sender's private key
hash_signature=$(echo "$message_hash" | openssl rsautl -sign -inkey "$sender/private.pem" | base64 -w 0);
signed_message="$message;$hash_signature"
if [[ $verbose ]]; then
    >&2 echo "Signed message is: $signed_message";
fi
echo "$signed_message";
